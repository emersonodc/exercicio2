class Cadastro < SitePrism::Page

    set_url "http://www.phptravels.net/admin"

    element :email,  "input[type='text'][placeholder='Email']"
    element :senha, "input[type='password'][name='password']"
    element :botao, "button.btn-primary[type='submit']"
    element :contas, "a[data-toggle='collapse'][href='#ACCOUNTS']"
    element :add, "button.btn-success"
    element :cp_nome, "input[placeholder='First name']"
    element :cp_snome, "input[placeholder='Last name']"
    element :email_cad, "input[type='email']"
    element :senha_cad, "input[type='password']"
    element :celular, "input[placeholder='Mobile Number']"
    element :endereco, "input[placeholder='Full address'][name='address1']"
    element :pais_sel, "span.select2-chosen"
    element :bt_submit, "button"

    
    
    def login (paran1,paran2)
        email.set(paran1)
        senha.set(paran2)
        botao.click
    end

    
   # def dados (paran1,paran2,paran3,paran4,paran5,paran6)
 #     cp_nome.set(paran1)
  ###    senha_cad.set(paran4)
     #   celular.set(paran5)
      #  endereco.set(paran6)
       # pais_sel.click
    #end
    #def pais(paran1)
       # pais_sel.set(paran1).click
    #end
        
    
end



