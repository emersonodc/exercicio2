#language: pt
#utf-8

Funcionalidade: Cadastrar Customer

Eu como administrador do sistema
Quero cadastrar um novo Customer
Para ter as informacoes em meu sistema
@cadastrar
Cenario: Realizar Cadastro
Dado que eu acesse o site como administrador
Quando preencher as informacoes necessarias
Entao o cadastro deve ser realizado com sucesso
