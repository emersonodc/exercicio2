Dado("que eu acesse o site como administrador") do
    Cadastro.new.load
    sleep 2
    Cadastro.new.login("admin@phptravels.com","demoadmin")
    sleep 5
    Cadastro.new.contas.click
    sleep 1
    click_link('Customers')
   
  end
  
  Quando("preencher as informacoes necessarias") do
    Cadastro.new.add.click
    #Cadastro.new.dados("Juriscleza","da Silva","ju.teste@teste.com",
      #"password123","551199999999", "Av. Paulista, 1900 - Sao Paulo")
    #Cadastro.new.pais("Brazil")
    Cadastro.new.cp_nome.set Faker::Name.name
    Cadastro.new.cp_snome.set Faker::Name.name
    Cadastro.new.email_cad.set Faker::Internet.email
    Cadastro.new.senha_cad.set Faker::Internet.password(8)
    Cadastro.new.celular.set Faker::PhoneNumber.cell_phone
    Cadastro.new.endereco.set Faker::Address.full_address
    Cadastro.new.pais_sel.click
    find(:xpath, '//*[@id="select2-drop"]/ul/li[20]/div').click
    Cadastro.new.bt_submit.click
      sleep 5
      
  end
  
  Entao("o cadastro deve ser realizado com sucesso") do
    assert_text('CHANGES SAVED!')
  end
  