Dado("que eu acesse o site") do
 Consulta.new.load
 Consulta.new.flights.click
 #Consulta.new.ins_partida.click


end
Quando("preencher as informacoes necessarias sobre a viagem") do
  Consulta.new.partida.click
  Consulta.new.ins_partida.set Faker::Address.country_code_long
  sleep 3
  find(:xpath, '//*[@id="select2-drop"]/ul/li[1]/div').click
  Consulta.new.destino.click
  Consulta.new.ins_destino.set Faker::Address.country_code_long
  sleep 3
  find(:xpath, '//*[@id="select2-drop"]/ul/li[1]/div').click
  
  Consulta.new.data_part.click
  Consulta.new.dt_part("2018-08-01")
  sleep 5
end

Entao("verei as informacoes sobre os voos pesquisados") do
 assert_text('DATES AVAILABILITY')
end