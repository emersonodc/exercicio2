#language: pt
#utf-8


Funcionalidade: Consultar Passagem Aerea

Eu como usuario do site
Quero consultar uma passagem aerea
Para ter as informacoes sobre voos
@passagens
Cenario: Consutar Passagens 
Dado que eu acesse o site 
Quando preencher as informacoes necessarias sobre a viagem
Entao verei as informacoes sobre os voos pesquisados
