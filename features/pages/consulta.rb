class Consulta < SitePrism::Page
    
    set_url "https://www.phptravels.net/"

    element :flights, "[href='#flights']"
    element :partida, "#s2id_location_from"
    element :ins_partida, "#select2-drop input.select2-input"
    element :destino, "div#s2id_location_to"
    element :ins_destino, "#select2-drop input.select2-input"
    element :data_part, "input.form-control.input-lg[name='departure']"
    element :botao_sub, "#flights button[type='submit']"



    def dt_part (paran1)
        data_part.set(paran1)
        botao_sub.click
    end
end